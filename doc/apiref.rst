API Reference
=============

This page lists the classes and methods that are necessary for building an analysis,
but are `not` related to expressions (see :doc:`Building expressions<treefunctions>`
for a description of those) |---| the aim is to provide a set of easy-to-use classes and methods.

Plots and selections
--------------------

.. automodule:: bamboo.plots
   :members:
   :special-members: __init__

Analysis modules
----------------

.. automodule:: bamboo.analysismodules
   :members:
   :special-members: __init__

Helper functions
----------------

.. automodule:: bamboo.analysisutils
   :members:

Scale factors
'''''''''''''

.. automodule:: bamboo.scalefactors
   :members:
   :special-members: __init__


.. |---| unicode:: U+2014
   :trim:

ROOT utilities
''''''''''''''

.. automodule:: bamboo.root
   :members:
